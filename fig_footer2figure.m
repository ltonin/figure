function fig_footer2figure(footer, varargin) 
% fig_footer2figure(footer, varargin)
%
% Add text on top of the current figure.varargin
% accepts all the default inputs of function text().
% In addition, the property pairs:
%       - 'Border', value       -> Border figure [default 0.06]
%       - 'BorderPlots', value  -> Border of single subplotes [default 0.1]
%
% Default arguments:
%
%       'FontSize', 6 
%       'VerticalAlignment', 'Top'
%       'HorizontalAlignment', 'left'
%
% Example:
%
% figure;
% fig_footer2figure('This is a test', 'Border', 0.05, 'BorderPlots', 0.01, 'FontSize', 8, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'Bottom', 'Interpreter', 'none', 'FontWeight', 'bold')


    default_border = 0.06;
    default_borderplots = 0.01;
    
    default_argin = {'FontSize', 6, 'VerticalAlignment', 'Top', 'HorizontalAlignment', 'left',};
    
    textargin = varargin;
    
    chk_border      = findcell_id(textargin, 'Border');

    
    if chk_border < 0
        border = default_border;
    else
        border = textargin{chk_border + 1};
        
        textargin(:, chk_border:chk_border+1) = textargin(:, end-1:end);
        
        textargin = textargin(:, 1:end-2);
        
    end
    
    chk_borderplots = findcell_id(textargin, 'BorderPlots');
    
    if chk_borderplots < 0
        border_plots = default_borderplots;
    else
        border_plots = textargin{chk_borderplots + 1};
        
        textargin(:, chk_borderplots:chk_borderplots+1) = textargin(:, end-1:end);
        
        textargin = textargin(:, 1:end-2);
        
    end
    
    axes('position', [border + border_plots, border/5, 1 - 2 * (border + border_plots), 4 * (border/5)], 'visible', 'off');
    
    total_argin = [default_argin textargin];
    text(0, 0, footer, total_argin{:});
    line([0, 1], [0, 0], 'Color', 'k');
end
