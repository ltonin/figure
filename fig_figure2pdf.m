function fig_figure2pdf(f, filename, orientation, resize)

    if nargin == 2
        orientation = [];
        resize      = [];
    elseif nargin == 3
        resize      = [];
    end
    
    if isempty(orientation)
        orientation = 'landscape';
    end
    
    if isempty(resize)
        resize = '-bestfit';
    end
    

    set(f,'PaperOrientation',   orientation);
    set(f,'PaperUnits', 'normalized');
    set(f,'PaperPosition', [0 0 1 1]);
    
    print(resize, f, '-dpdf', filename);
end


% %
% % fig_figure2pdf(f, filename, stylename)
% %
% % This function save a figure f in pdf format. It also crops the pdf.
% %
% % f             figure handle
% % filename      name of the output file ('*.pdf')
% % stylename     name of the style to apply to the figure. It must be
% %               already saved in the export dialog menu
% % [margins]     Optional: the 4 margins for the crop (left, top, right, bottom)
% %
% % Note: It works in unix platforms. epstopdf, pdfcrop must be installed.
% %
% % Example:
% %
% % % My style name: HighResolution
% % x = 0:0.1:100;
% % plot(x, sin(x));
% % fig_figure2pdf(gcf, 'myfig.pdf', 'HighResolution');
% %
% 
%     if nargin < 4
%         margin_commands = '';
%     else
%         margin_commands = ['--margins "' num2str(margins) '" '];
%     end
% 
%     [path, name, ext] = fileparts(filename);
%     
%     if ~strcmpi(ext, '.pdf')
%         error('chk:pdfext', 'The output file must be pdf')
%     end
%     
%     eps_filename        = [path '/' name '.eps'];
%     pdfcrop_filename    = [path '/' name '-crop' ext];
%     pdf_filename        = [path '/'     name ext];
% 
%     disp(['>> Applying style ' stylename]);
%     style = hgexport('readstyle', stylename);
%     
%     disp('>> Exporting eps');
%     hgexport(f, eps_filename, style);
%     
%     
%     %% Unix commands
%     
%     % Converting from eps to pdf (epstopdf)
%     disp('>> Converting to pdf');
%     conv_status = unix(['epstopdf ' eps_filename]);
%     
%     if conv_status ~= 0
%         error('chk:conv', 'Error converting to pdf');
%     end
%     
%     % Cropping the pdf (pdfcrop)
%     disp('>> Cropping pdf');
% %     crop_status = unix(['pdfcrop --margins 10 ' pdf_filename]);
%     crop_status = unix(['pdfcrop ' margin_commands pdf_filename]);
%     
%     if crop_status ~= 0
%         error('chk:conv', 'Error cropping the pdf');
%     end
%     
%     % Removing temporany files (rm)
%     disp('>> Removing temporany files')
%     rmpdf_status = unix(['rm ' pdf_filename]);
%     
%     if rmpdf_status ~= 0
%         error('chk:conv', 'Error removing the temp pdf');
%     end
%     
%     rmeps_status = unix(['rm ' eps_filename]);
%     
%     if rmeps_status ~= 0
%         error('chk:conv', 'Error removing the temp eps');
%     end
%     
%     % Renaming the final pdf file (mv)
%     mvpdf_status = unix(['mv ' pdfcrop_filename ' ' pdf_filename]);
%     
%     if mvpdf_status ~= 0
%         error('chk:conv', 'Error renaming the pdf');
%     end
%     
%     %% Output
%     disp(['>> Output pdf file: ' pdf_filename])
%     
%     
% end
