function [h] = fig_header2figure(f, text, vpos)
% [h] = fig_header2figure(f, text, height)
%
% Add horizontal centered string to the figure. Text appears always in the
% center of the figure. The default vertical position is 0.9. It can be
% changed with the optional argument vpos. vpos unit refere to the figure
% where 0 is bottom and 1 is the top.

    if nargin < 3
        vpos = 0.9;
    end

    h = annotation(f,'textbox',...
        [0 vpos 1 0.05],...
        'String',{text},...
        'HorizontalAlignment','center',...
        'VerticalAlignment','middle',...
        'FitBoxToText','off',...
        'LineStyle','none');

end
